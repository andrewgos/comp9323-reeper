var assert = require('assert');
var restify = require('restify');

//Create a JSON Client
var client = restify.createJsonClient('https://api.stackexchange.com/2.2');


var path = '/badges?order=desc&sort=rank&site=stackoverflow'
//Get call
client.get(path, function (err, req, res, obj) {
  assert.ifError(err);
  console.log(JSON.stringify(obj, null, 2));
  console.log('Server returned: %j', obj);
});
