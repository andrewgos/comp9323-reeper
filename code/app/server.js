/*
 * server.js
 * 
 * Server initialisation
 * and API routing
 * 
 * Note for Remote Deployment:
 * 1. Uncomment var swagger
 * 2. Uncomment CORS Setup
 * 3. Change var address to remote IP
 * 4. Change const WEB_URL in SO_auth to remote IP 
 * 
 * Author: Alvin Hielman
 * Date: 25 Oct 2015 (v1.2)
 * 
 */

var restify = require('restify');
//var swagger = require('swagger-ui')
var control = require('../controller/controller')



/***** SERVER INIT *****/

var port = '3000';
var address = '127.0.0.1';
//var address = '172.31.33.155';
var server = restify.createServer({name: 'REEPer'});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.listen(port, address, function() {
    console.log('%s listening @ %s', server.name, server.url);
});

//setup cors - swagger support
/*
restify.CORS.ALLOW_HEADERS.push('accept');
restify.CORS.ALLOW_HEADERS.push('sid');
restify.CORS.ALLOW_HEADERS.push('lang');
restify.CORS.ALLOW_HEADERS.push('origin');
restify.CORS.ALLOW_HEADERS.push('withcredentials');
restify.CORS.ALLOW_HEADERS.push('x-requested-with');
server.use(restify.CORS());
*/



/***** ROUTING *****/

var PATH = '';

PATH = '/account';
server.post({path: PATH + '/register'}, control.createAccount);
server.post({path: PATH + '/login'}, control.loginUser);
server.get({path: PATH + '/authenticate/stackoverflow'}, control.getSOAuthURL);
server.post({path: PATH + '/connect/stackoverflow'}, control.linkSOProfile);
//server.delete({path: PATH + '/delete'}, control.deleteAccount);


PATH = '/users';
server.get({path: PATH}, control.findAllUsers);
server.get({path: PATH + '/:userid'}, control.findUser);
server.get({path: PATH + '/:userid/:expertise'}, control.findUserScore);

PATH = '/update';
server.get({path: PATH + '/:userid'}, control.runUpdate);

PATH = '/process';
server.get({path: PATH + '/:userid'}, control.processUser);
server.get({path: PATH + '/:userid/:table'}, control.processUser);


PATH = '/platforms';
server.get({path: PATH}, control.getSupportedPlatform);


PATH = '/experts';
server.get({path: PATH + '/:expertise'}, control.findExperts);
server.get({path: PATH + '/:expertise/:name'}, control.findExpertsByLevel);

PATH = '/badge';
server.get({path: PATH + '/:badge'}, control.findBadge);

PATH = '/weights';
server.get({path: PATH + '/:table'}, control.getWeightTable);
server.post({path: PATH + '/create'}, control.createWeightTable);
server.put({path: PATH + '/update/:table'}, control.updateWeightTable);
server.put({path: PATH + '/setactive/:table'}, control.setWeightTable);



PATH = '/scoredata';
server.get({path: PATH + '/:expertise'}, control.getData); //global or expertise
