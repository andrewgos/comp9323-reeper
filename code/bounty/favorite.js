/**
 * Created by Michael on 10/2/2015.
 */
var request = require('request');
var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});

var question_id = 7563693;
var usr_favorite = 0;

request("http://stackoverflow.com/questions/"+question_id, function (err, response, body) {
    if (!err) {
        var fav = body.match("<div class=\"favoritecount\"><b>[0-9]+</b></div>");
        if (fav.length > 0) {
            usr_favorite = fav[0].replace("<div class=\"favoritecount\"><b>", "").replace("</b></div>", "");
            console.log(usr_favorite);
        }
    } else {
        console.log(err);
    }
});
