/**
 * Created by Udeeksh on 25/10/2015.
 */
/**
 * Created by Udeeksh on 24/10/2015.
 */
/*
 * This module contains hardcoded info about the expertiseArea and SO user Ids that need to be populated
 * */
var testInput = {
    /*"node.js":[419970,266795,893780],*/
    "java":[353852,1025118]
    /*"c++":[404970,743214],
     "python":[99594,41316,163740],
     "javascript":[3474494,572680],
     "android":[671543,1108032]*/
};

/*
 *   This method takes the above input data and flattens into a single array of tuples[expertiseArea, userId]
 *   Being done for convenience when getting and saving dataset in batch mode
 */

var flatTestData = function flattenTestInput(){
    var flatDataTuples = [] ;
    for (var expArea in testInput){
        //console.log("Exp Area: ",expArea);
        for (var id in testInput[expArea]){
            //var tuple = new Tuple();
            var tuple = [];
            tuple[0] = testInput[expArea][id];
            tuple[1] = expArea;
            tuple[expArea] = testInput[expArea][id];
            flatDataTuples.push(tuple);
        }
    }
    return flatDataTuples;
};
module.exports.input = testInput;
module.exports.flatInput = flatTestData;