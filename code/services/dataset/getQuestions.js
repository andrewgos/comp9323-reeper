/**
 * Created by Udeeksh on 2/10/2015.
 * -- 25.10.2015 Bug Fix
 *          -- Global variables retain the old values when module re-used; results are appended for dataset
 *          --- Solved by redeclaring all the required global vars inside the methods
 */

//var favCount = require('./favouriteAndBounty.js');
var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});
/*
 Method description:
 * Step 1: Fetch all the questions from StackOverflow for a given user
 * Step 2: Filter only those question that match the 'expertise area'
 * Step 3: Create the question List from these question, extracting all the simple features described in datasetUserSchema
 * Step 4: Return this list
 */
function getQues(quesQueryStr,expertiseArea, callback) {

    // Bug-Fix: Re-Initialize global vars
    var totalQues = 0;
    var questionList = [];
    var question = function(){};
    var quesIdList = [];

    service.get(quesQueryStr, function (err, req, res, obj) {
        if (err) {
            console.log("API Call failed !",err.toString());
            return callback(err);
        }
        for (var itemKey in obj.items) {
            totalQues++;
            // filter on the basis of tags
            //console.log("Tags: ",obj.items[itemKey].tags );
            if (obj.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                var ques = new question();
                //console.log("Found a question");
                ques['id'] = obj.items[itemKey].question_id;
                ques['score'] = obj.items[itemKey].score;
                ques['view_count'] = obj.items[itemKey].view_count;
                ques['is_answered'] = obj.items[itemKey].is_answered;
                ques['answer_count'] = obj.items[itemKey].answer_count;
                ques['active_duration'] = (obj.items[itemKey].last_activity_date - obj.items[itemKey].creation_date);
                quesIdList.push(ques.id);
                // Update : call the async functions to update favourite count:

                questionList.push(ques);
            }
        }
        // Update the favourite count of the selected questions:
        // TODO: need to make changes to the existing module to issue a single async call
       /* favCount.getQuestionBountyCount(quesIdList, function(err,quesFavCountMap){
            console.log("Question Ids looked for: ",quesIdList);
            console.log("Ques:FavCount Received: \n",quesFavCountMap);
        });*/
        //console.log("-Ques Id list: ",quesIdList);
        //console.log("-Total number of questions received for this API call: ", totalQues);
        console.log("-Fetched all questions successfully.");
        return callback(questionList);
    });
}
// Test and Debug
/*var questionQueryString = "/users/"+184046+"/questions?order=desc&max=999&sort=votes&site=stackoverflow";
getQues(questionQueryString,"python",function(response){
    console.log("Question Ids: ", response);
});*/

module.exports.getQues = getQues;
