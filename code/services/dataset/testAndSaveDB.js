/**
 * Created by Udeeksh on 4/10/2015.
 * Module is to do a simple batch processing for fetching a number of user's data from SO
 * The module invokes 'getUserData' to retrieve user data and has dummy DAO methods to test saving to the database.
 *
 * Update: Batch processing works by setting simple time out interval; DB still not working
 */
var mongoose = require('mongoose');
var userData = require('./getUserData.js');
var userHandle = require('./datasetUserSchema.js');
var defaultUserId = "266795";
var defaultExpertiseArea = "node.js";
var userSavedCtr = 0;

var dbRemoteServerIP = "52.10.183.118:27017";
mongoose.createConnection("mongodb://localhost/test");

// Dummy method to fetch save the data returned from 'getUserData.js'
function daoFetchAndSaveUser(userId, expertiseArea){
    userData.getData(userId, expertiseArea, function (userScoreObject) {
        console.log("From DAO:", userScoreObject);
        //checkDataReceived(userScoreObject); //Uncomment this function to see the detailed object

        // DB Stuff - Currently not working
         var dsObject = new userHandle.userModel(userScoreObject);
        console.log("Property of DB Object",dsObject,"\nAttempting to save.");
         dsObject.save(function (err){
             if (err){
                console.log("Couldn't save userId:",userId);
             }
             else {
                console.log("Data for userId:",userId,"Saved successfully.");
                userSavedCtr++;
                console.log("Total Users saved: ",userSavedCtr);
             }
         });
    });
}

// function to iterate through a list of user Ids and save their data
// batch processing currecntly not supported
userIds = [2957933,1035789,266795];
userIds = [266795];
expertiseArea = "node.js";
var ctr = 0;
function findSaveMultipleUsers() {
    console.log("Initiating a API Service call for User: ", userIds[ctr], "Wait = 5 seconds");
    daoFetchAndSaveUser(userIds[ctr], expertiseArea);
    ctr++;
    if (ctr < userIds.length) {
        setTimeout(findSaveMultipleUsers, 10000);
    }
}

findSaveMultipleUsers();

// Method to test validity of data received
function checkDataReceived(userScoreObject) {
    console.log("Data for ",userScoreObject.user_id);
    // Show questions:
    console.log("---: Questions :---");
    console.log(userScoreObject.expertise_area_list[0].question_list);
    console.log("---: Answers :---");
    console.log(userScoreObject.expertise_area_list[0].answer_list);
    console.log("---: Comments :---");
    console.log(userScoreObject.expertise_area_list[0].comment_list);
}

/* Tested with following user ids:
 userId = 1380918 - [user has many questions, answers and comments],
 userId = 295262
 userId = 1035789 - [ user has only 1 question, no relevant answer]
 userId = 2906872
 userId = 2957933

*/
