/**
 * Created by Udeeksh on 2/10/2015.
 * -- 25.10.2015 Bug Fix
 *              -- Global variables retain the old values when module re-used; results are appended for dataset 
 *              --- Solved by redeclaring all the required global vars inside the methods
 */
/*
 Method description:
 * Step 1: Fetch all the comments from StackOverflow for a given SO user
 * Step 2: From the list of comments returned, extract extract the parents posts
 * Step 3: Sub-categorize the parent posts into questions and answers
 *      Step 3.1: Issue API calls for answers and repeat steps described in 'getAnswers.js'
 *      Step 3.2: Issue API calls for questions and repeat steps described in 'getQuestions.js'
 *                  Note: only a subset of simple features is considered(eg. Score)
 * Step 4: Collate the results of all API calls into a single list of comments
 * Step 5: Return the list
 * Visual Representation of Comment API calls:
 *      getUserComment() -> Comment -> getParentPost() -- isQuestion --> getQuestion() --> Tag Matched -->Collate the results
 *                                                                                     --> Tag Not Matched --> Continue
 *                                                     -- isAnswer --> buffer the features --> getQuestion() --> Tag Matched --> Collate the results
 *
 */
var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});

function getComm (queryString, expertiseArea, callback) {

    // Bug-Fix: Re-Initialize global vars
    var Comment = function(){};
    var commentList = [];

    var postCommMap = {};
    var quesCommMap = {};
    var ansCommMap = {};
    var quesAnsMap ={};
    var quesIdList = "";
    var ansIdList = "";
    var quesQueryString;
    var ansQueryString;
    var totalComments = 0;

    service.get(queryString, function (err, req, res, obj) {
        if (err) {
            console.log("Could not make the API call to fetch user comments");
            return callback(err);
        }
        for (var itemKey in obj.items) {
            totalComments++;
            if (obj.items[itemKey].score > 0) {
                postCommMap[obj.items[itemKey].post_id] = [obj.items[itemKey].comment_id, itemKey];
                //console.log("Post:Comm==> ",obj.items[itemKey].post_id," : ",obj.items[itemKey].comment_id);
            }
        }
        //console.log("Total number of comments received for this API call: ", totalComments);
        var postIdList = Object.keys(postCommMap).join(';');
        var postQueryString = "/posts/" + postIdList + "?order=desc&max=100&sort=votes&site=stackoverflow";
        //console.log("post-Query String: ",postQueryString);
        service.get(postQueryString, function (err, req, res, obj_p) {
            // Determine if posts are questions or answers
            for (var itemKey in obj_p.items) {
                if (obj_p.items[itemKey].post_type == "question") {
                    quesCommMap[obj_p.items[itemKey].post_id] = postCommMap[obj_p.items[itemKey].post_id];
                } else {
                    ansCommMap[obj_p.items[itemKey].post_id] = postCommMap[obj_p.items[itemKey].post_id];
                }
            }
            //console.log(quesCommMap);
            quesIdList = Object.keys(quesCommMap).join(';');
            quesQueryString = "/questions/" + quesIdList + "?order=desc&sort=votes&site=stackoverflow";
            //console.log("quesIds: ",quesQueryString);
            ansIdList = Object.keys(ansCommMap).join(';');
            ansQueryString = "/answers/" + ansIdList + "?order=desc&sort=votes&site=stackoverflow";
            //console.log("ansIds: ",ansQueryString);
            // Filter comments as per questions and answers
            service.get(quesQueryString, function (err, req, res, obj_q) {
                for (var itemKey in obj_q.items) {
                    if (obj_q.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                        // update the comment
                        var commKey = quesCommMap[obj_q.items[itemKey].question_id][1];
                        var comment = new Comment();
                        comment['id'] = obj.items[commKey].comment_id;
                        comment['score'] = obj.items[commKey].score;
                        comment['parent_id'] = obj_q.items[itemKey].question_id;
                        comment['parent_score'] = obj_q.items[itemKey].score;
                        commentList.push(comment);
                    }
                }
                //console.log("Comment List after analyzing comment->Post->Question\n", commentList);
                // Bug Fix - 25.10.2015 
                if (Object.keys(ansCommMap).length == 0 ){
                    console.log("No answers parents found");
                    return callback(commentList);
                }
                // Filter the answers through their respective questions
                service.get(ansQueryString, function (err, req, res, obj_a) {
                    quesIdList = "";
                    for (var itemKey in obj_a.items) {
                        quesIdList += (obj_a.items[itemKey].question_id + ";")
                        quesAnsMap[obj_a.items[itemKey].question_id] = [obj_a.items[itemKey].answer_id, itemKey];
                    }
                    quesQueryString = "/questions/" + quesIdList.slice(0, quesIdList.length - 1) + "?order=desc&sort=votes&site=stackoverflow";
                    //console.log("Post: Comments = ",postCommMap);
                    //console.log("new ques query string",quesQueryString);
                    // Final API call: Look for questions and their tags:
                    service.get(quesQueryString, function (err, req, res, obj_qq) {
                        // Error Handling
                        if (err){
                            console.log("Error in fetching comment->answer->question");
                            return callback(error);
                        }
                        // Was obj_q before:: maybe a bug
                        for (var itemKey in obj_qq.items) {
                            if (obj_qq.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                                // update the comment
                                var ansKey = quesAnsMap[obj_qq.items[itemKey].question_id][1];
                                //TODO Possible Bug: Need to check further
                                var commKey = postCommMap[quesAnsMap[obj_qq.items[itemKey].question_id][0]];
                                var comment = new Comment();
                                comment['id'] = obj.items[commKey[1]].comment_id;
                                comment['score'] = obj.items[commKey[1]].score;
                                comment['parent_id'] = obj_a.items[ansKey].answer_id;
                                comment['parent_score'] = obj_a.items[ansKey].score;
                                commentList.push(comment);
                            }
                        }
                        //console.log("Comment List after analyzing comment->Post->Answer->Question\n", commentList);
                        console.log("---Fetched all comments Successfully.");
                        return callback(commentList);
                    });
                });
            });
        });
    });
}

//Debug
/*getComm(commentQueryString,expertiseArea,function(response){
 console.log("All the relevant comments are---\n",response);
 });*/

module.exports.getComms = getComm;
