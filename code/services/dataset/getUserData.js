/**
 * Created by Udeeksh on 2/10/2015.
 */

var ques = require('./getQuestions.js');
var ans = require('./getAnswers');
var comm = require('./getComments');
var userModel = require('./datasetUserSchema.js');

/*
Function to get question,answer and comment data from respective modules and create the user object
Input: Stack Overflow User Id(userId), Expertise Area
Method Description:
    Step 1: Fetches all Questions
    Step 2: Fetch all Answers
    Step 3: Fetch all Comments
    Step 4: Create the datasetUser object
Output: Return final object to be inserted in database
*/
function getUserData( userId, expertiseArea,callback) {
    //module.exports.getData = function getUserData( userId, expertiseArea,callback) {

    var questionQueryString = "/users/"+userId+"/questions?order=desc&max=999&sort=votes&site=stackoverflow";
    ques.getQues(questionQueryString, expertiseArea, function (questionList) {

        //console.log(questionList);
        var answerQueryString = "/users/"+userId+"/answers?order=desc&max=999&sort=votes&site=stackoverflow";
        ans.getAns(answerQueryString, expertiseArea, function (ansList) {

            //console.log("Answer List: \n",ansList);
            var commentQueryString = "/users/"+userId+"/comments?order=desc&max=999&sort=votes&site=stackoverflow";
            comm.getComms(commentQueryString, expertiseArea, function (commList) {

                //console.log("Comment List: \n",ansList);
                var finalData = createFinalObject(userId, expertiseArea, questionList, ansList, commList);

                callback(finalData);
            });
        });
    });
};
module.exports.getData = getUserData;
// Function prepares the final object in accordance with the dataset user schema.
// Refer @datatsetUserSchema.js
function createFinalObject(userId,expertiseArea,questionList,ansList,commList){
    var expertiseAreaObj = new Object();
    var userScoreObject = new Object();
    userScoreObject['user_id'] = userId;
    userScoreObject['location'] = "default";
    userScoreObject['expertise_area_list'] = [];

    expertiseAreaObj['name'] = expertiseArea;
    expertiseAreaObj['question_list'] = questionList;
    expertiseAreaObj['answer_list'] = ansList;
    expertiseAreaObj['comment_list'] = commList;
    userScoreObject.expertise_area_list.push(expertiseAreaObj);
    userScoreObject['badge_list'] = [{}]; // TODO: refers to the SO badges
    //console.log("____Final Object____\n",userScoreObject);
    return userScoreObject;
}

// Test and Debug
/*
userId = 1380918;
expertiseArea = "node.js";
getUserData(userId,expertiseArea,function(finalData){
   console.log("Received data for ",userId);
});
*/
