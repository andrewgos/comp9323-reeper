
The module usage is described as follows:
* Single User scenario
testAndSaveDB --> getUserData -|--> getUserQues
                              -|
                              -|--> getUserAns
                              -|
                              -|--> getUserComm
                              -|
                              
  Module is generating user objects as per the schema described in 'datasetUserSchema.js'.
  
  Current Status: 13.10.2015
    -> getUserData is working correctly;
    -> dbConnections and saving data is not working yet [FIXED]
    -> need to add support for batch processing in sequence [ADDED]
    
  Current Status: 25.10.2015
    -> batch processing modules added - can add 35 users at a time before SO blocks
* Batch Processing scenario

batchInput
   |
   V
batcProc <--> getUserData <=|--> getUserQues
   |                       -|
   |                       -|--> getUserAns
   V                       -|
Database                   -|--> getUserComm
(userModels)  
