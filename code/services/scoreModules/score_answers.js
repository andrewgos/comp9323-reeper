/**
 * Created by Udeeksh on 29/09/2015.
 */
/*Required Input : Hardcoded right now*/
var userId = "266795";
var expertiseArea = "node.js";
/*Note:*/

var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});

//var answerQueryString = "/users/"+userId+"/answers?order=desc&max=999&sort=votes&site=stackoverflow";;

var answerScore = {
    total_answers: 0,
    accepted_answers: 0,
    unaccepted_answers: 0,
    total_questions: 0,
    total_score: 0,
    question_score:0,
    total_implied_views: 0,
    total_activity_period: 0
};
var totalAnswers = 0;
var answerIds = [];
var quesAnsMap = {};
var ansIdItemKeyMap = {};
var quesIdList = "";
// These two variables will be used for storing inherited values
var filteredAnswerList = [];
var filteredAnswer = {
    question_id: 0,
    answer_id: 0,
    question_score: 0,
    question_view_count: 0,
    question_activity_period: 0
};
//console.log("Searching for User Id: ",userId, " in expertise area: ",expertiseArea);
var testCtr = 0;
/*
 Method description:
 * Step 1: Fetch all the answers from StackOverflow for a given user
 * Step 2: From the list of answers returned, extract questions Ids and create a new query string
 *      Step 2.1 : Fetch all the questions from and filter them with the tags
 *      Step 2.2 : Add the corresponding filtered answer Ids. These are the inherited attributes.
 *      Step 2.3 : Update the derived attributes in 'filteredAnswer' and create an array for all such objetcs
 * Step 3 : Update the question score object according to the question_score schema using the 'filteredAnswers' for inherited attributes
 */
// API call to fetch all the answers for a particular user
exports.getAnswerData = function(userid, expertisearea) {
    var answerQueryString = "/users/"+userid+"/answers?order=desc&max=999&sort=votes&site=stackoverflow";;
    console.log("Searching for User Id: ",userid, " in expertise area: ",expertisearea);

    service.get(answerQueryString, function(err, req, res, obj) {
        for(var itemKey in obj.items){
            totalAnswers++;
            quesAnsMap[obj.items[itemKey].question_id] = obj.items[itemKey].answer_id ;
            ansIdItemKeyMap[obj.items[itemKey].answer_id] = itemKey;
        }
        console.log("Total number of answers received for this API call: ",totalAnswers);

        // Filter the relevant answers based on their question tag
        quesIdList = Object.keys(quesAnsMap).join(';');
        var newQueryString = "/questions/"+quesIdList+"?order=desc&sort=votes&site=stackoverflow";
        console.log(newQueryString);
        // Second API call to fetch all the questions for the answers and filter as per tag
        service.get(newQueryString, function(err, req, res, obj_q) {
            for(var itemKey in obj_q.items) {
                // Filter according to tags and update inherited attributes
                if (obj_q.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                    answerIds.push(quesAnsMap[obj_q.items[itemKey].question_id]);
                    filteredAnswer = new Object();
                    filteredAnswer.answer_id = quesAnsMap[obj_q.items[itemKey].question_id];
                    filteredAnswer.question_id = obj_q.items[itemKey].question_id;
                    filteredAnswer.question_view_count =  obj_q.items[itemKey].view_count;
                    filteredAnswer.question_score = obj_q.items[itemKey].score;
                    filteredAnswer.question_activity_period = obj_q.items[itemKey].last_activity_date - obj_q.items[itemKey].creation_date;
                    filteredAnswerList.push(filteredAnswer);
                }
            }
            console.log("Number of relevant answers: ", answerIds.length, " Loop ran : ",testCtr);
            //Final update using filtered answers
            for ( var answerKey = 0; answerKey < filteredAnswerList.length; answerKey++) {
                // Retrieve the relevant item key for answers
                var itemKey = ansIdItemKeyMap[filteredAnswerList[answerKey].answer_id];
                // Update the answerScore object
                answerScore.total_answers++;
                answerScore.total_score +=  obj.items[itemKey].score;
                answerScore.question_score+= filteredAnswerList[answerKey].question_score;
                answerScore.total_implied_views += filteredAnswerList[answerKey].question_view_count;
                answerScore.total_activity_period+= filteredAnswerList[answerKey].question_activity_period;//
                if (obj.items[itemKey].is_accepted){
                    answerScore.accepted_answers++;
                } else {
                    answerScore.unaccepted_answers++;
                }
            }
            console.log("Aggregate Answer score :",answerScore);
        });
    });
    
    return filteredAnswer;
};