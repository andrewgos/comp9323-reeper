/*
 * score_misc.js
 * 
 * Modules to get question's favourite count
 * and question's bounty by screen scraping
 * question html page.
 * 
 * Why screen scraping? Because currently
 * there's no way to find favourite and 
 * bounty count of a question through
 * Stackoverflow API 
 * 
 * Author: Alvin Hielman
 * Date: 8 Oct 2015 (v1.1)
 * 
 */

var request = require('request');
var url = 'http://stackoverflow.com/questions/';


exports.getQuestionFavCount = function(questionIds, callback) {
	var obj = {};
	questionIds.forEach(function(questionId, index, array) {
		request(url +questionId, function(err, res, body) {
			if(err) return callback(err);
			
			var count = 0;
			var match = body.match('<div class="favoritecount"><b>[0-9]+</b></div>');
			
			if(match)
				count = parseInt(match[0].replace(/[a-zA-Z="></ ]/g, ''));
			
			obj[questionId] = count;
			
			if(Object.keys(obj).length == array.length)
				callback(err, obj);
		})
	});
}

exports.getQuestionBountyCount = function(questionIds, callback) {
	var obj = {};
	questionIds.forEach(function(questionId, index, array) {
		request(url +questionId, function(err, res, body) {
			if(err) return callback(err);
			
			var count = 0;
			var match = body.match('<span class="bounty-award"[a-zA-Z0-9=" ]*>\\+[0-9]+</span>');
			
			if(match)
				count =  parseInt(match[0].replace(/<[a-zA-Z0-9-=" ]*>\+/, '').replace('</span>', ''));
			
			obj[questionId] = count;
			
			if(Object.keys(obj).length == array.length)
				callback(err, obj);
		})
	});
}
