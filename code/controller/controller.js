/*
 * controller.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 18-9-2015
 * 
 * Changes:
 *  Alvin Hielman - 10 Oct 2015 20:00 AEST
 *      + Code refactor for consistency.
 *      + Added HTTP response to all account functions.
 *      + Made all account callback to be an error first callback.
 *  Alvin Hielman - 12 Oct 2015 16:30 AEST
 *      + Made all users and badges callback to be an error first callback.
 *      + Added missing import and fixed compile error from other imports.
 */

var dao = require('../model/dao')
var proc = require('../data/processor');
var account = require('../login/login');
var authSO = require('../SO_auth/SO_auth');
var User = require('../model/schemas/user');
var Badge = require('../model/schemas/badge');
var Platform = require('../model/schemas/platform');
var ScoredataSchema = require('../model/schemas/scoredata');
var Weights = require('../data/weights');
var WeightTables = require('../model/schemas/weighttable');
var UserScore = require('../model/schemas/userscores');
var services = require('../services/dataset/getUserData');
var dataSchema = require('../services/dataset/datasetUserSchema');


/***** WEIGHTS *****/

//Get the selected weight table for configuration name
exports.getWeightTable = function (req, res, next) { //
    dao.find_one(WeightTables, {configuration: req.params.table}, function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404, 'No table Found');
        
        return next();
    });
};

//Update the selected weight table with the new parameter weights
exports.updateWeightTable = function (req, res, next) {
    dao.find_one(WeightTables, {configuration: req.params.table}, function(result) {
        if (result == null) {
            res.send(404, 'No table found');
            return next();
        } else {
            Weights.updateTable(req.body, result, function(ok) {
                if (ok)
                    res.send(200, 'Table updated');
                return next();
            });
        }
    });
};

//Set the given weight table active for the platform
exports.setWeightTable = function (req, res, next) {
    dao.find_one(WeightTables, {configuration: req.params.table}, function(result) {
        if(result == null) {
            res.send(404, 'No table found');
            return next();
        } else {
            dao.update(WeightTables, {active: true}, {active: false}, function (result) { //set the active to false
                dao.update(WeightTables, {configuration: req.params.table}, {active: true},function (result) {
                    if (result)
                        res.send(200, 'Weight table set');
                    return next();
                });
            });
        }
    })
};

//Create a weight table with the given configuration - if parameters are not given default ones are submitted
exports.createWeightTable = function (req, res, next) {
    dao.insert(Weights.buildTable(req.body), function(result) {
        if(result)
            res.send(201, 'Weight table created');
        else
            res.send(400,'Failed to create table');

        return next();
    });
};


/***** SCOREDATA *****/

exports.getData = function (req, res, next) {
    dao.find_one(ScoredataSchema, {expertise: req.params.expertise}, function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'Score Data Not Found');

        return next();
    });
};


/***** Platform (Metadata) *****/

exports.getSupportedPlatform = function (req, res, next) {
    dao.find_all(Platform, {}, function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'No platform found');
        
        return next();
    });
}


/***** USERS *****/

exports.findAllUsers = function (req, res, next) {
    dao.find_all(User, {limit: 20, sort: {badge_ids: -1}}, function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'No users found');
        
        return next();
    });
};

exports.findUser = function (req, res, next) {
    dao.find_one(User, {user_id: req.params.userid}, function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'User Not Found');
        
        return next();
    });
};

exports.findUserScore = function (req, res, next) {
    dao.find_one(UserScore, {user_id: req.params.userid, expertise: req.params.expertise}, function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'User score not found');
        
        return next();
    });
};

/***** SEARCH (EXPERTS) *****/

exports.findExperts = function (req, res, next) {
    // TODO: Implement filter by platform - waiting for schema to accomodate this.
    dao.find_all_of(Badge, {expertise: req.params.expertise},  function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'Experts not found');

        return next();
    });
};

exports.findExpertsByLevel = function (req, res, next) {
    dao.find_one(Badge, {expertise: req.params.expertise, name: req.params.name}, function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'Experts not found');

        return next();
    });
};


exports.findBadge = function (req, res, next) {
    console.log(req.params.badge);
    dao.find_one_id(Badge, req.params.badge,  function(result) {
        if(result)
            res.send(200, result);
        else
            res.send(404,'Badge not found');

        return next();
    });
};

/***** DATASET (UPDATING) *****/


/**
 * Runs the entire service for a user - fetches the account and calls the stackoverflow API
 * for the users data. The SO dataset is then processed through the complete platform.
 * A connect for endpoints and manual calls, designed for new users.
 * @param userid - the platform uid - a user is required (and expected) in the database
 * @param callback
 */
var createUser = function(userid, callback) {
    dao.find_one(User, {user_id: userid}, function (result) {
        /*
         We fetch the data using the SOid and the everything else is done by the internal id
         The services call the Stackexchange API
         */
        var defaultExpArea = 'node.js'; // TODO : needs to process a list of supported areas
        services.getData(result.SO_id, defaultExpArea,  function (dataset_object) {
            var blob = new dataSchema.userModel(dataset_object);
            if(dataset_object['expertise_area_list'][0]['question_list'].length < 1 &&
                dataset_object['expertise_area_list'][0]['answer_list'].length < 1) {
                return callback()
            }

            //Create the dataset in the db
            dao.insert(blob ,function () {
                //Run the processor module and generate all the user data from the dataset - only use active tables
                proc.generateData(userid, dataset_object, '', function(ok){
                    //Note the time of the
                    dao.update(User, {user_id: userid}, {update: Date.now()}, function (result) {
                        console.log('User updated')
                        return callback();
                    });
                });
            });
        });

    });
};

module.exports.createNewDataSet = createUser;

/**
 * Run a user update calling the API resource
 */
exports.runUpdate = function(req, res, next) {
    dao.find_one(User, {user_id: req.params.userid}, function(result) {
        if (result) {
            /*
             We fetch the data using the SOid and the everything else is done by the internal id
             The services call the Stackexchange API
             */
            var defaultExpArea = 'node.js'; // TODO : needs to process a list of supported areas
            services.getData(result.SO_id, defaultExpArea, function (dataset_object) {
                if(dataset_object['expertise_area_list'][0]['question_list'].length < 1 &&
                    dataset_object['expertise_area_list'][0]['answer_list'].length < 1) {
                    res.send(400, 'Call failed')
                    return next();
                }
                var blob = new dataSchema.userModel(dataset_object);
                //Create the dataset in the db
                dao.replace(dataSchema.userModel, blob, {user_id: dataset_object.user_id}, function () {
                    //Run the processor module and generate all the user data from the dataset - only use active tables
                    proc.generateData(req.params.userid, dataset_object, '', function (ok) {
                        //Note the time of the
                        dao.update(User, {user_id: req.params.userid}, {update: Date.now()}, function (result) {
                            console.log('User updated')
                            res.send(200, 'Updated');
                        });
                    });
                });
            });
        } else {
            res.send(404, 'User not found');
        }
        return next();
    });
};


/**
 * Regenerate the user data based of the stored dataset and scoring information
 * Designed for testing weight and badge settings
 */
exports.processUser = function(req, res, next) {
    dao.find_one(User, {user_id: req.params.userid}, function (result) {
        if(result) {
            dao.find_one(dataSchema.userModel, {user_id: result.SO_id}, function (dataset_object) {

                //Run the processor module and generate all the user data from the dataset
                proc.generateData(req.params.userid, dataset_object, req.params.table, function (ok) {
                    res.send(200, 'User updated');
                });
            });
        } else {
            res.send(404, 'User not found');
        }
        return next();
    });
};


/***** ACCOUNT *****/

exports.createAccount = function(req, res, next) {

    var email = req.body.email;
    var surname = req.body.surname;
    var username = req.body.username;
    var password = req.body.password;
    var givenName = req.body.givenName;
    var displayName = req.body.displayName;
    
    if(!email || !username || !password) {
        res.send(400, "Please fill in the required field (email, username, password)");
        return next();
    }
    
    account.createAccount(givenName, surname, username, email, password, displayName, function(err, isCreated, userid) {
        if(err)
            res.send(400, err.message);
        else if(isCreated)
            res.send(201, "Account Created with User ID: "+ userid);
        
        return next();
    });
};

exports.loginUser = function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    
    // TODO: Username is currently optional in createAccount, how to login?
    // User can login with username or email
    account.login(username, password, function(err, key) {
        if(err){
            res.send(400, err.message);
            return next();
        }
        
        res.send(200, "Login successful - Your token: " +key);
        
        return next();
    });
};

exports.deleteUser = function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;

    // TODO: Username is currently optional in createAccount, how to login?
    // User can login with username or email
    account.remove(username, password, function(err) {
        if(err){
            res.send(400, err.message);
            return next();
        }

        res.send(200, "Account: " + username + " is successfully deleted.");

        return next();
    });
};


exports.getSOAuthURL = function(req, res, next) {
    authSO.getAuthURL(function(err, url) {
        if(err)
            res.send(err);
        else
            res.send(200, "Please approve our app at: " +url);
        
        return next();
    });
};

exports.linkSOProfile = function(req, res, next) {
    var token = req.body.token;
    var SO_code = req.body.SO_code;
    var userid = req.body.userid;

    // get user data from database
    dao.find_one(User, {user_id: userid, tempLoginToken: token}, function(result) {
        if(!result) {
            res.send(400, 'Please verify your userid and token.');
            return next();
        }

        // check if login token is expired
        var currentDate = new Date();
        if(result.expiry < currentDate){
            res.send(400, 'Login Token Expired. Please re-login.');
            return next();
        }

        // link the profile
        authSO.linkProfile(userid, SO_code, function(err) {
            if(err) {
                res.send(400, err);
                return next();
            }

            // call badge module
            createUser(userid, function() {
                res.send(200, "Stackoverflow Account Linked Successfully to this userid:"+userid);
                return next();
            });
        });
    });
};
