/**
 * Created by alex on 17/10/15.
 */

var javascript_experts = [3474494,572680,219661,887539,2435473,451969,1461008,571194,548696,893780,1906307,1726343,552438,
    663031,448296,854246,613198,1669279,21886,62082,462627,166491,2025923,305644,235710,1435655,491075,610573,258127,126562,
    1848600,584192,186535,1586880,527185,1317805,201078,741747,1113426,1957251,1059101,785065,1259510,135978,23283,1001985,
    2324107,850833,34088,20670,605744,240443,548225,1719752,3001736,1211528,251311,246342,1529630,1210329,697154,167735,20128,
    362536,995579,246616,381345,156142,182705,763246,90511,871050,785541,20862,353802,1026459,1175966,1209018,1172002,298053,
    1230836,128035,1835379,75525,411591,339852,96656,851498,229044,3085310,23528,1668533,283863,1583,2224265,1903116,1229023,
    1715579,592746,266795];

var nodejs_experts = [419970,617839,266795,1259510,893780,201952,568109,62082,785065,15031,2050455,295262,1348195,1380918,
    1380918,1348195,295262,2050455,15031,785065,62082,568109,201952,893780,1259510,266795,617839,11926,419970,1380918,1348195,
    295262,2050455,15031,785065,62082,568109,201952,893780,1259510,266795,617839,11926,419970];

var python_experts = [99594,41316,163740,2487184,1730674,1461210,795990,400617,3058609,901925,1600898,282912,653364,248296,
    146792,4099593,1014938,565635,34855,450128,3923281,102441,68063,3337070,3297613,873145,704848,2867928,229602,1265154,
    918959,541136,594589,168352,54017,8747,2581969,953482,779200,117092,128141,2096752,2296458,142637,388787,21945,521034,
    208880,444036,2141635,1126841,140185,832621,391531,1217358,2073595,16148,267781,1628832,566644,31676,1199589,351031,
    156771,320726,249341,654031,71522,113962,1763356,380231,200291,984421,434217,1405065,47773,24718,644898,254617,3001761,
    392949,1350899,711085,10293,322806,704327,34088,102302,2357112,9530];

var cplusplus_experts = [404970,743214,174614,279259,149392,121747,3723423,16406,64960,221955,1329652,131930,2455888,178761,
    60281,3002139,868014,506413,1670129,3959454,496161,2296458,95382,50617,402169,3093378,21886,4301306,528724,68587,
    41747,768469,33345,212858,2069064,3953764,3204551,501250,2380830,3647361,25507,1566221,1053968,1505939,1870232,
    1938163,649233,434551,1141471,592323,47453,565635,1413395,186997,1009479,2756719,366904,3510483,481267,2684539,
    541686,743382,46821,1119701,1090079,2509,388520,923854,283302,9611,775806,493122,420683,1838266,1737,1760345,82294,
    1322972,399317,8922,865874,435800,143305,3937,812912,225074,256138,172531,4323,1708801,1168156];

var csharp_experts = [446279,838807,298053,201088,115413,21567,102112,159145,362218,431359,231316,164392,111575,661933,
    224370,11492,121493,1828879,411632,3764814,437768,479659,3032289,659190,1036390,1207195,4574,10263,108796,913124,
    369,479512,3538012,35245,668272,134204,706456,74015,314291,573218,719034,373321,26095,53771,232593,121309,879420,
    83264,987,591656,109702,12752,1870803,114029,580053,861716,885318,44743,5728,352552,40521,2319407,10174,245860,
    89556,785745,517852,87399,930847,1783619,468973,2060725,220643,30225,1849109,318425,249281,779513,993547,1559,
    570150,69809,1136211,808486,5620,30155,409259,2311360,1768303,97385,743382,20481,961526,634824,8707,643085,199245,857807];

var android_experts = [671543,1108032,3249477,1331415,763080,1253844,154306,1207921,2649012,857361,1561247,1531054,
    2183804,1590502,906362,927034,1839336,448625,2016562,2558882,760489,599346,375874,1741671,477754,139985,1114338,
    721079,548568,1221571,327402,82788,1027277,969325,758104,1109425,931982,420015,543539,450534,533552,675383,440010,
    1103682,878126,2985303,557179,1676363,247013,1064728,1235698,1286723,527288,1327899,492694,630668,241475,384306,
    1631193,294248,995891,593709,385478,542091,550966,179850,693752,786718,426429,1815485,132047,101361,429063,170842,
    769265,236136,1029225,1380752,580556,624069,1333975,546999,833622,903469,473070];

var ios_experts = [632339,30618,877465,675170,1375695,1219789,937822,43832,250728,916299,8427,603977,640731,3418066,
    197143,1445366,1106035,669586,420303,491239,291788,2998,508595,957768,2021193,343340,550177,205185,1917782,421018,
    1730272,803787,294949,201863,427083,104790,6330,983912,147019,41116,1280373,544050,608157,299924,3737,49658,220643,
    1395941,846273,1104384,412916,341750,451475,74118,400056,106435,1633251,418715,1155387,171206,1988185,716216,91282,
    981049,1285133,457406,427309,309925,422133,341994,467105,573626,1187415,137350,115730,427332,335858,97337,1226963,
    1271826,116862,671858,945847,852828,191596,25646,582,491980,77567,643383,19679];

var objectivec_experts = [10673,171206,421018,13000,201863,803787,1917782,205185,294949,76559,1312143,608157,467105,
    427083,276622,159827,30618,201199,147019,168225,716216,3737,945847,23649,632339,49658,846273,568283,981049,309925,
    343340,1285133,1988185,451475,296387,1395941,299924,250728,74118,1271826,341994,19410,182676,104200,190807,165059,
    14256,427332,116908,104790,400056,227765,458390,1226963,1187415,50122,106435,412916,17279,427309,457406,852828,
    671858,6330,265530,239243,573626,335858,418715,41116,491980,28804,77567,19679,603977,643383,191596,582,10320,34218,
    557219,169346,116862,97337,220819,6694,137350,224671,2140,30461,50742,115730,25646];

var database_experts = [27535,9034,13302,1144035,55159,20860,135152];//TODO redo "database" instead of mongodb

var java_experts = [353852,1350762,1025118,20938,18122,723920,1768232,964243,3636601,6509,642706,341508,204788,179630,
    4856258,635608,1202025,1798593,685641,589259,731620,581205,2189127,44330,1850609,2670892,699224,2891664,473070,870248,
    501696,116472,1237040,489261,1705598,1064245,2193767,61624,122428,1441122,2670792,573032,2422776,844882,44523,506855,
    171061,3182664,8753,2541560,1870555,466862,263004,100565,472792,2959,1869846,2353911,749588,653856,2711488,68587,
    185541,1221571,1864167,300311,70795,116509,1449199,1225328,27198,3218114,1339987,41423,1048330,1076640,2300597,
    343568,179233,1704529,72673,35501,2970947,121993,21925,616460,31480,49246,166339];


var experts_list = {
                    'node.js'     : nodejs_experts,
                    'java'        : java_experts,
                    'python'      : python_experts,
                    'C++'         : cplusplus_experts,
                    'C#'          : csharp_experts,
                    'android'     : android_experts,
                    'ios'         : ios_experts,
                    'objective-c' : objectivec_experts,
                    'database'    : database_experts,
                    'javascript'  : javascript_experts
                    };



//var ctrl = require('../controller/controller');
var dao = require('../model/dao');
var userSchema = require('../model/schemas/user');
var dataset = require('../services/dataset/getUserData');
var datasetSchema = require('../services/dataset/datasetUserSchema');
var datasetuser = 'dataset_';
var counter = 1;
var proc = require('../data/processor');


/** SETTINGS **/

var FETCHLIMIT = 2;// nodejs_experts.length;

var expertise = 'node.js';


/******** INJECTION ONLY - CALL SO & INSERT *******/

function inject(soid, exp, callback) {
    dataset.getData(soid, exp, function (dataset_object) {
        if(dataset_object) {
            var blob = new datasetSchema.userModel(dataset_object);
            console.log(dataset_object['expertise_area_list'][0]['question_list']);
            //dao.insert(blob, function () {
            //    return callback();
            //})
            return callback();
        }
    });
};

function injectDatasets(i) {
    if (i < FETCHLIMIT) {
        var soid = experts_list[expertise][i];

        inject(soid,expertise, function() {
            injectDatasets( i+1);

        })

    }
}

/******** BUILDS MINED SCORESDATA - requires - DATASETS ONLY *******/

function buildScoreDataFromDB() {
    dao.find_all(datasetSchema.userModel, {}, function(result) {
        ScoreDataFromDB(0, result);
    })
}

function ScoreDataFromDB(i, dataset_object) {
    if (i < dataset_object.length) {
        proc.processDataOnly('', dataset_object[i], '', true, true, function (ok) {
            console.log('Processed: '+dataset_object[i].user_id);
            ScoreDataFromDB(i+1, dataset_object);
        });
    }
}

/******* BUILD USERS ***/

function buildUsers(i) {

    if (i < FETCHLIMIT) {

        var soid = experts_list[expertise][i];
        var user = datasetuser + (i + 1);
        console.log(soid+' '+user);
        var new_user = new userSchema({
            user_id: user,
            tempLoginToken: null,
            expiry: Date.now(),
            SO_id: soid,
            SO_access_token: null,
            type: 'stackoverflow',
            badge_ids: []
        });
        insertNewUser(new_user, expertise, function (ok) {
            buildUsers(i + 1);
        });
    }
}

function insertNewUser(new_user, expertise, callback) {
    dao.insert(new_user, function (a) {
        log(a);
        doData(new_user, expertise, function (ok) {
            return callback(ok);
        });
    })
}

function doData(new_user, exp, callback) {

    //Full API calls to stackoverflow
    dataset.getData(new_user.SO_id, exp, function (dataset_object) {
        if (dataset_object) {
            var blob = new datasetSchema.userModel(dataset_object);
            dao.insert(blob, function () {
                proc.generateData(new_user.user_id, dataset_object, '', function (result) {
                    return callback(result)
                });
            });
        }
    });


}

/***** SCORE USERS - builds badges and userscores - requires Users+ dataset ***/

function scoreUsers(i) {

    if (i < FETCHLIMIT) {

        var soid = experts_list[expertise][i];
        var user = datasetuser + (i + 1);

        runScores(user, soid, function (ok) {
            scoreUsers(i + 1);
        });
    }
}

function runScores(new_user, soid, callback) {

    dao.find_one(datasetSchema.userModel, {user_id: soid}, function (dataset_object) {
        if (dataset_object != null) {
            proc.generateData(new_user, dataset_object, '', function (ok) {
                return callback(ok)

            });
        }
    });
}


/******** BUILD SCORES - RUNS PROCESSOR - LOADS ALL USES AND DATASETS ********/
function fetchUsersForProcessing() {
    dao.find_all(userSchema, {limit: 3}, function(result) {
        processUsers(0, result);
    })
}

function processUsers(i, result) {

    if (i < result.length) {
        //console.log(result[i]);

        var soid = result[i].SO_id;
        var userid = result[i].user_id;
        runAllScores(userid,soid, function (ok) {
            processUsers(i + 1, result);
        });
    }
}


function runAllScores(userid,soid, callback) {

    dao.find_one(datasetSchema.userModel, {user_id: soid}, function (dataset_object) {
        if (dataset_object != null) {
            proc.processDataOnly(userid, dataset_object, '', function (ok) {
            //proc.generateData(userid, dataset_object, '', function (ok) {
                return callback(ok)

            });
        }
    });
}

/********* CREATE USERS FROM DATAMODELS *********/

function fetchModelsForProcessing() {
    dao.find_all(datasetSchema.userModel, {limit: 20}, function(result) {
        console.log(result);
        buildUsers(0, result);
    })
}

function buildUsers(i, result) {

    if (i < result.length) {

        var soid = result[i].user_id;
        var user = 'datasetuser_' + (i + 1);
        console.log(soid+' '+user);
        var new_user = new userSchema({
            user_id: user,
            tempLoginToken: null,
            expiry: Date.now(),
            SO_id: soid,
            SO_access_token: null,
            type: 'stackoverflow',
            badge_ids: []
        });
        console.log(new_user);
        dao.insert(new_user, function (a) {
             buildUsers(i + 1, result);
        })
    }
}

//Util
function deleteAllUsersBadges() {
    //deleting only the users
    dao.update(userSchema, {}, {badges_ids: []}, { multi: true }, function (result) {
        console.log(result);
    })
}

var test = function(){
    //buildUsers(0);
    //injectDatasets(0);
    //buildScoreDataFromDB(0);
    //scoreUsers(0);
    //deleteAllUsersBadges();
    fetchUsersForProcessing();
    //fetchModelsForProcessing();
    //buildScoreDataFromDB();
};

//Run
test();


function log(msg) {
    var logging = true;
    if(logging)
        console.log(msg);
}

