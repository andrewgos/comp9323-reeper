var dao = require('../model/dao');
var assert = require('assert');

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
//var testSchema = require('testSchema');

//Dummy schema for dao
var testModel = new Schema({
   userid: Number,
   name: String
});
var Tester = mongoose.model('Test',testModel)


var newtest = new Tester({
    userid : 123,
    name : "new_user"
});


function runTest() {
     dao.insert(newtest, function(result) { //this will create multiple entries under different keys
         console.log(result);
         var id = result['_id'];
            dao.find_one_id(Tester, id, function(id_found) {
                console.log(id_found);
                assert.notEqual({'_id' :id_found}, null, 'Failed!');
                
                dao.find_all(Tester, {}, function(found_all) {
                    assert.notEqual(found_all, null, 'Failed!');
                    
                    dao.find_one(Tester, {userid: 123}, function(obj_find) {
                        assert.notEqual(obj_find, null, 'Failed');
                        console.log("Find one OK");
                        
                        dao.find_one(Tester, {userid: 1223}, function(not_found) {
                            assert.equal(not_found, null, 'Failed');
                            console.log("Not found OK");
                            
                            dao.update(Tester, {userid: 123}, {name: 'newer_user'}, function(updated) {
                                dao.find_one(Tester, {userid: 123}, function(update_found) {
                                    assert.notEqual(obj_find.name, 'new_user', 'Failed');
                                    
                                    dao.remove_id(Tester, id, function() {
                                            console.log('Test complete - data wiped');
                                            process.exit(1);
                                    });

                                });
                            });

                        });
                    });
                });
            });
      }); 
    
};

runTest();

