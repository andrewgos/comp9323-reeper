/**
 * Created by Michael on 9/28/2015.
 * Install: npm install stormpath
 * It is required to create account in stormpath and then store keyxxx.properties
 * For more information go to 
 * www.stormpath.com
 * http://docs.stormpath.com/nodejs/quickstart/
 */
var stormpath = require('stormpath');
var keyfile = 'C:/Stormpath/' + 'apiKey-39TH48LGIETF5TMZLT5CVAJH0.properties';
client = null;

stormpath.loadApiKey(keyfile, function apiKeyFileLoaded(err, apiKey){
    if (err) throw err;
    client = new stormpath.Client({apiKey: apiKey});

    client.getApplications({name:'My Application'}, function(err, applications){
        if (err) throw err;
        app = applications.items[0];

        // Search for Account by email.
        app.getAccounts({email: 'budis@stormpath.com'}, function(err, accounts) {
            if (err) throw err;
            accounts.each(function (account, index) {
                console.log(account.givenName + " " + account.surname);
                console.log(account.customData.href);

                client.getAccount(account.customData.href, function(err,acc){
                    if (err) throw err;
                    console.log(acc.displayname);
                });
            });
        });
    });

});


