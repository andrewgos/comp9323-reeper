/*
 * service_score_misc.js
 * 
 * Module to test /services/score_misc.js
 * Test cases are correct as of:
 * 8 Oct 2015 18:00 AEST
 * 
 * Author: Alvin Hielman
 * Date: 3 Oct 2015
 * 
 */

var assert = require('assert');
var misc = require('../services/score_misc');

var questionFavIds = ['359699', '20547819', '5400163', '2835559', '208193'];
misc.getQuestionFavCount(questionFavIds, function(err, obj) {
	// console.log(obj);
	console.log('Starting "getQuestionFavCount" test...');
	assert(25 == obj['359699']);
	assert(107 == obj['20547819']);
	assert(103 == obj['5400163']);
	assert(112 == obj['2835559']);
	assert(101 == obj['208193']);
	console.log('Test complete');
})

var questionBountyIds = ['32477734', '1222281', '21911168', '26707618', '8869485'];
misc.getQuestionBountyCount(questionBountyIds, function(err, obj) {
	// console.log(obj);
	console.log('Starting "getQuestionBountyCount" test...');
	assert.equal(50, obj['32477734']);
	assert.equal(100, obj['1222281']);
	assert.equal(500, obj['21911168']);
	assert.equal(250, obj['26707618']);
	assert.equal(200, obj['8869485']);
	console.log('Test complete');
})