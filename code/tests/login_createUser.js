/**
 * Created by Michael on 9/28/2015.
 * Install: npm install stormpath
 * It is required to create account in stormpath and then store keyxxx.properties
 * For more information go to 
 * www.stormpath.com
 * http://docs.stormpath.com/nodejs/quickstart/
 */
var stormpath = require('stormpath');
var client = null;
//var homedir = (process.platform === 'win32') ? process.env.HOMEPATH : process.env.HOME;
//var keyfile = homedir + '/.stormpath/apiKey.properties';
var keyfile = 'C:/Stormpath/' + 'apiKey-39TH48LGIETF5TMZLT5CVAJH0.properties';

stormpath.loadApiKey(keyfile, function apiKeyFileLoaded(err, apiKey) {
    if (err) throw err;
    client = new stormpath.Client({apiKey: apiKey});

    console.log('Created client!');

    // Get the Stormpath Application.
    client.getApplications(function(err, apps) {
        if (err) throw err;

        var app = apps.items[0];

        console.log('Application retrieved!');
        
        /*
        var account = {
            givenName: 'Joe',
            surname: 'Stormtrooper',
            username: 'tk455_2',
            email: 'stormtrooper_2@stormpath.com',
            password: 'Changeme1',
            customData: {
                favoriteColor: 'white',
            },
        };
        */
        var account = {
            givenName: 'Budi',
            surname: 'Suwandi',
            username: 'Guan',
            email: 'budis@stormpath.com',
            password: 'Changeme1',
            customData: {
                favoriteColor: 'white',
                displayname: 'CoolUsr',
            },
        };
        
        // Create a Stormpath Account.
        app.createAccount(account, function(err, account) {
            if (err) throw err;
            console.log('Account created!');
            console.log('Account givenName: ' + account.givenName);
            console.log('Account surname: ' + account.surname);
        });
    });
});
