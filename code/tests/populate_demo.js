var hat = require('hat');
var dao = require('../model/dao');
var User = require('../model/schemas/user');
var control = require('../controller/controller');

var dummyUserOne = new User({
	user_id : hat(),
	SO_id: '1380918',
	type: 'SO'
});

dao.insert(dummyUserOne, function() {
	console.log('Dummy user 1 created: ' +dummyUserOne.user_id);
});

control.updateUser(dummyUserOne.user_id);



// var dummyUserTwo = new User({
	// user_id : hat(),
	// SO_id: '266795',
	// type: 'SO'
// });

// dao.insert(dummyUserTwo, function() {
	// console.log('Dummy user 2 created: ' +dummyUserTwo.user_id);
// });

// control.updateUser(dummyUserTwo.user_id);
