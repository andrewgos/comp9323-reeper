/**
 * Created by alex on 17/10/15.
 */

var scoreDataSchema = require('../model/schemas/scoredata');
var dao = require('../model/dao');


var MAX = 0;
var TOTAL = 1;
var COUNT = 2;


/**
 * Get the requested scoredata from the DB. Either an expertise area or the unique Global data
 * @param exp - expertise
 * @param callback
 */
exports.getScoreData = function(exp, callback){
    dao.find_one(scoreDataSchema, {expertise: exp}, function(data) {
        if(data == null) {
            data = createscoreData(exp);
            dao.insert(data, function() {
                console.log('Initialised default score data');
                return callback(data);
            });
        } else {
            return callback(data);
        }
    });
};

/**
 *  Accumulation totals of the new data with the existing
 * @param segment - the question part (q/a/c)
 * @param key
 * @param value
 * @param scoredata
 * @param callback
 * @returns callback(scoredata)
 */
exports.scoreDataTraining = function(segment, key, value, scoredata, callback) {

    if(scoredata[segment+'list'][key] == null)
        return callback(scoredata);
    if(key == 'is_answered' || key == 'is_accepted') {
        if(value) { //is boolean
            scoredata[segment+'list'][key][1]++;
        } else {
            scoredata[segment+'list'][key][0]++;
        }
    } else {
        //score etc..
        if(value) {
            scoredata[segment+'list'][key][TOTAL] += value;
            scoredata[segment+'list'][key][COUNT]++;
        }
        if(value > scoredata[segment+'list'][key][MAX])
            scoredata[segment+'list'][key][MAX] = value;
    }

    return callback(scoredata);
};

/**
 * Write out the updated scoredata to the database
 * @param data
 */
exports.updateScoreData = function (data, callback) {
    dao.update(scoreDataSchema, { expertise: data.expertise },
        {
            question_list : {
                score           : data.question_list.score,
                view_count      : data.question_list.view_count,
                is_answered     : data.question_list.is_answered,
                answer_count    : data.question_list.answer_count,
                active_duration : data.question_list.active_duration
            },
            answer_list :{
                score                    : data.answer_list.score,
                is_accepted              : data.answer_list.is_accepted,
                parent_score             : data.answer_list.parent_score,
                parent_view_count        : data.answer_list.parent_view_count,
                parent_active_duration   : data.answer_list.parent_active_duration
            },
            comment_list : {
                score          : data.comment_list.score,
                parent_score   : data.comment_list.parent_score
            }
        }
        , function(result) {
            return callback();
        });
};

/*
    Initialise the default scoredata
 */
function createscoreData(name) {
    return new scoreDataSchema({
            expertise: name,
            question_list : {
                score           : [0,0,0], //[Max, Total, Count]
                view_count      : [0,0,0],
                is_answered     : [0,0], //[Count_0, Count_1]
                answer_count    : [0,0,0],
                active_duration : [0,0,0]
            },
            answer_list :{
                score                    : [0,0,0], //[Max, Total, Count]
                is_accepted              : [0,0], //[Count_0, Count_1]
                parent_score             : [0,0,0],
                parent_view_count        : [0,0,0],
                parent_active_duration   : [0,0,0]
            },
            comment_list : {
                score          : [0,0,0], //[Max, Total, Count]
                parent_score   : [0,0,0]
            }
        });
}