/*
 * processor.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 6-10-2015
 */


var dao = require('../model/dao');
var UserScore = require('../model/schemas/userscores');
var BadgeSchema = require('../model/schemas/badge');
var UserSchema = require('../model/schemas/user');
var weightTables =  require('./weights');
var scoreData = require('./scoredata');
var complex = require('./complex');
var assert = require('assert');


//Globals
var _expList = 'expertise_area_list';
var _segments = ['question_','answer_','comment_'];
var _expertiseArea = '';
var _current_expertise_scoredata = null;
var _global_scoredata = null;
var _weight_tables = null;
var _data_object = null;
var _uid = '';
var _expertise_sub_scores = {};
var _complex_data = {};
var _processDataOnly = false;
var _minedata = false;
var _new_complex_badges = null;
var testing = false;

/**
 * Generate all the data and users scores and results. This is called by the controller
 * and is passed the user dataset object for data extraction and collection
 * @param uid - the platform user id
 * @param obj - the dataset object
 * @param useTable - the custom table parameter
 * @param callback
 */
exports.generateData = function(uid, obj, useTable, callback) {
    console.log('Updating: '+uid+' '+Date());

     //setup some global params
     _uid = uid;
     _data_object = obj;

    //Get the global scoredata - this keeps data collection of the entire platform regardless of expertise area
    scoreData.getScoreData('Global_data', function(data) {
        _global_scoredata = data;

        //Load the active weight tables
        weightTables.getWeightTables(useTable, function(table) {
            _weight_tables = table;
            getExpertise(0, function(result){
                console.log('Completed update '+Date());
                return callback(result);
            });
        });
    });
};

/**
 * Processes the data from the dataset only. No user is updated - for injected data dump/large user volumes
 * @param userid
 * @param dataset - the dataset object
 * @param useTable - weigting table
 * @param callback
 */
exports.processDataOnly = function(userid, dataset, useTable, callback) {
    _processDataOnly = true;
    _minedata = false;
    this.generateData(userid, dataset, useTable, function(){
        return callback();
    });
};

function getExpertise(index, callback) {

    if(index < _data_object[_expList].length) {

        _expertiseArea = _data_object[_expList][index]['name'];

        //Load the existing scoredata for the expertise field
        scoreData.getScoreData(_expertiseArea, function(expertise_data) {

            _current_expertise_scoredata = expertise_data;

            processor(index, function(result) {
                if(_processDataOnly) {

                    getExpertise(index+1, callback);

                } else {
                    writeUserExpertiseScores(function(end){
                        getExpertise(index+1, callback);
                    });
                }
            });
        });
    } else {
        return callback('OK done');
    }
}

/**
 The processor - takes the complete expertise subset of the dataset for the user and
 extracts the compiled information
 1. The scoredata for the global data and expertise data are both updated with all user
 information from the users dataset
 2. Sub scores for each segment (questions, answers, comments, etc) are calculated
 with the active weight table
 3. The sub scores are passed on for scoring and collected data is written out to the database

 */

function processor(index, callback) {
    _expertise_sub_scores = {};
    _complex_data = {};

    //processSegments(index, 0, function() {
    processSegments(index, 0);

    //Compile the final accumulated complex_data from all the users data

    complex.evaluation(_complex_data, null, function(badges) {
        _new_complex_badges = badges;
        scoreData.updateScoreData(_current_expertise_scoredata, function (ok) {
            scoreData.updateScoreData(_global_scoredata, function (result) {

                return callback(result);
            });
        });
    });
    //});
}
//Synchronous looping
function processSegments(index, i) {
    if(i < _segments.length){
        _expertise_sub_scores[_segments[i]] = 0; //init
        processDataparts(index, i, 0);
    }
}

function processDataparts(index, i, j) {
    if(j < _data_object[_expList][index][_segments[i] + 'list'].length) {

        var dataset = _data_object[_expList][index][_segments[i] + 'list'][j];

        var table = _weight_tables[_segments[i] + 'list'];
        table = table.toObject();
        dataset = dataset.toObject();
        for (var key in dataset) {

            if(_minedata) {
                //We have the field, fetch the weight information from the db and update all the weights
                scoreData.scoreDataTraining(_segments[i], key, dataset[key], _global_scoredata, function(ob) {
                    _global_scoredata = ob;
                    scoreData.scoreDataTraining(_segments[i], key, dataset[key], _current_expertise_scoredata, function(obj) {
                        _current_expertise_scoredata = obj;

                        /** Sub score weighting */
                        if (table[key] != null) {
                            _expertise_sub_scores[_segments[i]] += dataset[key] / 100 * table[key]
                        }

                    });
                });
            } else {
                /** Sub score weighting */
                if (table[key] != null) {
                    _expertise_sub_scores[_segments[i]] += dataset[key] / 100 * table[key]
                }
            }
        }
        //We process all the data parts individually
        _complex_data = complex.processPart(_complex_data, _current_expertise_scoredata, dataset, _segments[i]);

        //Synchronous looping
        processDataparts(index,i,j+1);
    } else {
        processSegments(index, i+1)
    }
}

/**
 * Write all the score data out to the database and updating the score counts for the user
 */
function writeUserExpertiseScores(callback) {


    var questionScore =  _expertise_sub_scores['question_'];
    var answerScore   =  _expertise_sub_scores['answer_'];
    var commentScore  =  _expertise_sub_scores['comment_'];

    var userScoreData = null;
    //Fetch the users score data for the current expertise
    dao.find_one(UserScore, {userid: _uid, expertise: _expertiseArea }, function(result) {
        userScoreData = result;

        //If they have no data for the current expertise create the new data template
        if(userScoreData == null) {
            userScoreData = new UserScore({
                userid: _uid,
                expertise: _expertiseArea,
                scoredata: {
                       question_score: [questionScore, 1],  //[Total, count]
                       answer_score:   [answerScore,   1],
                       comment_score:  [commentScore,  1],

                       complex_feature: _new_complex_badges
                }
            });
            addComplexBadges([], function (data) {
                //Add the users new expertise data
                dao.insert(userScoreData, function () {
                    log('DB: Created new scores for uid: ' + _uid + ' expertise: ' + _expertiseArea + '\n')
                    badger(userScoreData, function (result) {
                        return callback(result);
                    });
                });
            });
        } else {

            //If the users score data already exists, add the new data to the existing scores
            if (questionScore) { //[Count, Total]
                userScoreData['scoredata']['question_score'][1]++;
                userScoreData['scoredata']['question_score'][0] += questionScore;
            }
            if (answerScore) { //[Count, Total]
                userScoreData['scoredata']['answer_score'][1]++;
                userScoreData['scoredata']['answer_score'][0] += answerScore;
            }
            if (commentScore) { //[Count, Total]
                userScoreData['scoredata']['comment_score'][1]++;
                userScoreData['scoredata']['comment_score'][0] += commentScore;
            }

            addComplexBadges(userScoreData['scoredata']['complex_feature'], function (data) {
                userScoreData['scoredata']['complex_feature'] = data;
                //Save out the update data
                dao.update(UserScore, {userid: _uid, expertise: _expertiseArea},
                    {scoredata: userScoreData['scoredata']}, function () {
                        log("Score data updated " + _uid);
                        badger(userScoreData, function (result) {
                            return callback(result);
                        });
                    });

            })

        }
    });
}
function addComplexBadges(existing_complex_badges, callback) {

    if(_new_complex_badges.length < 1)
        return callback();
    checkBadges(0, existing_complex_badges, function(data) {
        return callback(data);
    });
}

function checkBadges(index, existing_complex_badges, callback) {
    if(index < _new_complex_badges.length) {
        checkNewBadges(index, 0, existing_complex_badges, function(new_badge) {
            if(new_badge) {
                updateBadgeData(new_badge.name, function(ok) {
                    existing_complex_badges.push(new_badge);
                    checkBadges(index+1,  existing_complex_badges, callback);
                });
            } else {
                checkBadges(index+1,  existing_complex_badges, callback);
            }

        });
    } else {
        return callback(existing_complex_badges);
    }
}


function checkNewBadges(i, j,  existing_complex_badges, callback) {
    if(j < existing_complex_badges.length) {
        if(existing_complex_badges[j].name == _new_complex_badges[i].name)
            return callback();
        checkNewBadges(i, j+1, existing_complex_badges, callback);
    } else {
        return callback(_new_complex_badges[i]);
    }
}

/**
 * The Badger: Accumulates all the users expertise data and compares it against all the
 * gather expertise data by all users to data. So the average scoring by the user
 * (positive scores are counted only) is ranked against the collected data.
 * @param userScoreData
 * @param callback
 */
function badger(userScoreData, callback) {
    var final_score = 0;

     final_score += (userScoreData['scoredata']['question_score'][0] / userScoreData['scoredata']['question_score'][1])
     / 100 * _weight_tables['question_score'];
     final_score += (userScoreData['scoredata']['answer_score'][0] / userScoreData['scoredata']['answer_score'][1])
     / 100 * _weight_tables['answer_score'];
     final_score += (userScoreData['scoredata']['comment_score'][0] / userScoreData['scoredata']['comment_score'][1])
     / 100 * _weight_tables['comment_score'];

    var expFinalDataScore = 0;

    scored(0, expFinalDataScore, function(expFinalDataScore) {

        var disc = null;

        /*
         Find out what percentage is final score of the expertise final score.
         If the users is x% greater or less than the accumulated average for that
         expertise area then they are awarded a rank
         */
        console.log('Final: '+final_score+' expData: '+expFinalDataScore);

        var rank = (final_score * 100) / expFinalDataScore;

        console.log('rank: ' + rank); //remove me


        if (rank > 150) //Calibration needed - waiting for data
            disc = 'Expert';
        if (rank < 150 && rank > 100)
            disc = 'Proficient';
        if (rank < 100 && rank > 50)
            disc = 'Skilled';
        if (rank < 50 && rank > 5)
            disc = 'Talented';

        if(disc) {
           updateBadgeData(disc,  function (result) {
                return callback(result);
           });
        } else {
            return callback('No badge awarded');
        }
    });

}

//Building the scoring information for all users from the database
//Used to compare and score users data against
function scored(i, expFinalDataScore, callback) {
    if(i < _segments.length) {
        var expScoreData = 0;
        var table = _weight_tables[_segments[i] + 'list'];
        var scores = _current_expertise_scoredata[_segments[i]+'list'];
        table = table.toObject();

        for(var key in table) { //[Max, Total, Count]

            /** Score weighting */
            if (key != 'is_answered' && key != 'is_accepted') { // accepted/answered - complex features
                //We are averaging out the sub level scores before weighting them together
                expScoreData += (scores[key][1] / scores[key][2]) / 100 * table[key];
            }

        }
        //We mixed the weighted averages together based on the same final weights the users score is done at
        expFinalDataScore += expScoreData / 100 * _weight_tables[_segments[i]+'score'];
        scored(i+1, expFinalDataScore, callback);
    } else {

        return callback(expFinalDataScore);
    }
}

/**
 Update the Badge if it already exists for the current expertise area with the user id.
 The Badge id is also added to the users list of awarded badges and the level that they were awarded.
 Currently users are awarded multiple levels of badges for each expertise.
 These are given a date of award and users can track their progress
 */
function updateBadgeData(disc, callback) {
    //Fetch the existing badge matching the expertise area and the level description
    dao.find_one(BadgeSchema, {expertise: _expertiseArea, name: disc}, function(badge) {
        var badgeid = '';
        //The badge doesn't exist so we can make a new now
        if(badge == null) {
            badge = BadgeSchema({
                userids: [_uid],
                expertise: _expertiseArea,
                description: complex.descriptions[disc] || 'Expertise badge for ' + _expertiseArea,
                name: disc

            });
            //Create the new badge in the database
            log(badge);
            dao.insert(badge, function (badgeData) {
                badgeid = badgeData._id;
                log("Created badge");

                //Update the user to contain the badge
                dao.find_one_update(UserSchema, {user_id: _uid},
                    {$addToSet: {badge_ids: {badge: badgeid, awarded: Date.now()}} },
                    function() {
                        log('User badge added');
                        return callback('All done');
                    });
            });

            //If the badge does exist in the database we want to add the user
        } else {
            //Check to see if the user already has the badge

            if(badge.userids.indexOf(_uid) == -1) {
                log(badge);
                badgeid = badge._id;
                //Update the badge to contain the user
                dao.find_one_update(BadgeSchema, {_id: badgeid},
                    {$addToSet: {userids: _uid}}, function(result) {
                    log('Badge updated')
                    //Award the badge to the user, success!
                    dao.find_one_update(UserSchema, {user_id: _uid},
                        {$addToSet: {badge_ids: {badge: badgeid, awarded: Date.now()}} }, function(result) {
                            log('User badge added');
                            return callback('All done');
                        });
                });
            } else {
                return callback('All done');
            }
        }
    });
}


function log(msg) {
    var logging = false;
    if(logging)
        console.log(msg);
}
