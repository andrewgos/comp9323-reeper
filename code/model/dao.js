/*
 * dao.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 17-9-2015
 */


var mongoose = require('mongoose');
var conn = require('./connection');



conn.createDBconnection();


/*****  Create  *****/

exports.insert = function(model, cb) {
    model.save(function(err, docs) {
        if (err) 
            console.trace('Insert: '+err);
        return cb(docs);
    });
};

/****** Search *****/

exports.find_all_of = function(schema, conditions, cb) {
    schema.find(conditions, null, {}, function(err, docs) {
        if(err)
            console.trace('Find all: '+err);

        return cb(docs);
    });
};


/*****  Find  *****/

exports.find_all = function(schema, options, cb) {   
    schema.find({}, null, options, function(err, docs) {
        if(err)
            console.trace('Find all: '+err);
        
        return cb(docs);
    });
};


exports.find_one = function (schema, conditions, cb) {
    schema.findOne(conditions, function (err, docs){
        if(err) 
            console.trace('Find one: '+err);
        return cb(docs);
    });
};

exports.find_one_id = function (schema, id, cb) {
    schema.findById(id, function (err, docs){
        if(err)
            console.trace('Find id: '+err);
        
        return cb(docs);
    });
};

/*****  Updates  *****/

exports.update = function (schema, conditions, update, cb) {
    schema.update(conditions, update, { multi: false }, function (err, docs) {
        if (err) 
            console.trace('Update: '+err);
        return cb(docs);
    });
};


exports.find_one_update = function (schema, conditions, update,  cb) {
    schema.findOneAndUpdate(conditions, update, {upsert: true, safe: true}, function(err, docs) {
        if(err)
            console.trace('Find one update: '+err);
        return cb(docs);
    });
};

exports.update_model = function (model,  cb) { //normally use other updates
    model.save(function(err, docs) {
        if(err)
            console.trace('Update model: '+err);
        return cb(docs);
    });
};

exports.replace = function(schema, model, conditions, cb) {
    schema.remove(conditions, function(err, docs) {
        if(err) console.trace('Replace '+err);
        model.save(function(err,docs) {
            if(err) console.trace('Replaced '+err);
            return cb(docs);
        })
    })
};

/*****  Delete  *****///Util

exports.remove_id = function(schema, id,cb) {
    schema.findByIdAndRemove(id, {}, function(err, docs) {
        if(err)
            console.trace('Remove id: '+err);
        return cb(docs);
    });
};

exports.find_one_remove = function (schema, conditions, cb) {
    schema.remove(conditions, function(err, docs){
        if (err) console.trace('Find one delete: ' + err);
        return cb(docs);
    });
};
