
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*
 * Stored weights for learning
 * - as users information is collected min/max/average levels are modified
 */ 
var weightSchema = new Schema({

        //Global
        name: String, //Global, expertise
        question_score: Array,  //[Min, Max, Total, Count, Weight]
        question_view_count: Array, 
        question_is_answered: Array,  //[count_0, count_1, Weight] 
        question_answer_count: Array,
        question_active_duration: Array,

        answer_score: Array,

        comment_score: Array,
    
});


//Compile the models
var WeightSchema = mongoose.model('Weights', weightSchema);
module.exports = WeightSchema;

