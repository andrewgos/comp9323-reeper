
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
   
var dataSchema = new Schema({
                
                userid           : String,
                location         : String,
                expertise        : String,
                
                question_list    : Array, //JSON
                /*
                   	"id": "",
                   	"score": "",
                   	"view_count": "",
                   	"is_answered": "",
                    "answer_count": "",
                    "active_duration": ""
                */
                answer_list      : Array, //JSON
                /*
                	"id": "",
                    "score": "",
                    "is_accepted": "",
                	"parent_id”: “”,
                	“parent_score”: “”,
                    "parent_view_count": "",
                    "parent_active_duration": ""
                */

                comment_list    : Array, //JSON
                /*
                    "id": "",
                    "score": “”,
                    "parent_id”: “”,
                    "parent_score”: “”,
                */
                badge_list       : Array //JSON
                /*
                    "id": "",
                    "name": "",
                    "rank": "",
                    "type": "",
                    "award_count": ""
                */
});

;


var scoreSchema = new Schema({
    expertise_area : String,
    question_score : String, //JSON
    /*
        	"total_questions": "",
        	"answered_questions": "",
        	"unanswered_questions": "",
        	"total_answers": "",
        	"total_score": "",
        	"total_views": "",
        	"total_activity_period": ""
    */
    answer_score   : String, //JSON
    /*
        	"total_answers": "",
        	"accepted_answers": "",
        	"unaccepted_answers": "",
        	"total_questions": "",
        	"total_score": "",
        	"total_implied_views": "",
        	"total_activity_period": ""
    */
    comment_score  : String //JSON
    /*
            "total_score": "",
        	"post_score": "",
    */
});

//Compile the models
var DataSchema = mongoose.model('DataSchema', dataSchema);
var ScoreSchema = mongoose.model('ScoreSchema', scoreSchema);
module.exports = DataSchema;
//module.exports = ScoreSchema;

