/*
 * user.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 17-9-2015
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*
The permitted SchemaTypes are
    String
    Number
    Date
    Buffer
    Boolean
    Mixed
    ObjectId
    Array
*/


var userSchema = new Schema({
  user_id           : String,
  tempLoginToken    : String,
  expiry            : { type: Date},
  SO_id             : String,
  SO_access_token   : String,
  created           : { type: Date, default: Date.now },
  updated           : { type: Date},
  type              : String,
  badge_ids         : Array
});

//Compile the model
var User = mongoose.model('User', userSchema);

module.exports = User;


    
